<?php

namespace App\Entity;

use App\Repository\OpinionsRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=OpinionsRepository::class)
 */
class Opinions
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $text;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $authorName;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $authorSurname;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $authorAddress;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $authorCity;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $authorProvince;

    /**
     * @ORM\Column(type="integer")
     */
    private $authorPostalcode;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $authorCountry;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(string $text): self
    {
        $this->text = $text;

        return $this;
    }

    public function getAuthorName(): ?string
    {
        return $this->authorName;
    }

    public function setAuthorName(string $authorName): self
    {
        $this->authorName = $authorName;

        return $this;
    }

    public function getAuthorSurname(): ?string
    {
        return $this->authorSurname;
    }

    public function setAuthorSurname(string $authorSurname): self
    {
        $this->authorSurname = $authorSurname;

        return $this;
    }

    public function getAuthorAddress(): ?string
    {
        return $this->authorAddress;
    }

    public function setAuthorAddress(string $authorAddress): self
    {
        $this->authorAddress = $authorAddress;

        return $this;
    }

    public function getAuthorCity(): ?string
    {
        return $this->authorCity;
    }

    public function setAuthorCity(string $authorCity): self
    {
        $this->authorCity = $authorCity;

        return $this;
    }

    public function getAuthorProvince(): ?string
    {
        return $this->authorProvince;
    }

    public function setAuthorProvince(string $authorProvince): self
    {
        $this->authorProvince = $authorProvince;

        return $this;
    }

    public function getAuthorPostalcode(): ?int
    {
        return $this->authorPostalcode;
    }

    public function setAuthorPostalcode(int $authorPostalcode): self
    {
        $this->authorPostalcode = $authorPostalcode;

        return $this;
    }

    public function getAuthorCountry(): ?string
    {
        return $this->authorCountry;
    }

    public function setAuthorCountry(string $authorCountry): self
    {
        $this->authorCountry = $authorCountry;

        return $this;
    }
}
