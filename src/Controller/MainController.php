<?php

namespace App\Controller;

use App\Entity\Formulario;
use App\Entity\Guardians;
use App\Entity\Opinions;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class MainController extends AbstractController
{
    /**
     * @Route("", name="home")
     */
    public function home(Request $request, EntityManagerInterface $doctrine, LoggerInterface $logger)
    {
        $repoOpiniones = $doctrine->getRepository(Opinions::class);
        $repoGuardianes = $doctrine->getRepository(Guardians::class);
        $repoFormulario = $doctrine->getRepository(Formulario::class);


        $opiniones = $repoOpiniones->findAll();
        $guardianes = $repoGuardianes->findAll();
        $formularioReg = $repoFormulario->findAll();

        if ($request->getMethod() === 'POST') {

            $form = new Formulario();
            $form->setName($request->request->get('name'));
            $form->setEmail($request->request->get('email'));
            $form->setTimetable($request->request->get('timetable'));
            $form->setCity($request->request->get('city'));
            $form->setSpaceavailable($request->request->get('spaceavailable'));
            $form->setPrivacy(true);

            return $this->render("formFill.html.twig", ['form' => $form]);
        }

        return $this->render("home.html.twig", [
            'opiniones' => $opiniones,
            'guardianes' => $guardianes,
            'FormularioReg' => $formularioReg
        ]);
    }

    /**
     * @Route("/opinion", name="addComentario")
     */
    public function formAddComentario(Request $request, EntityManagerInterface $doctrine)
    {
        if ($request->getMethod() === 'POST') {

            $op = new Opinions();

            $op->setText($request->request->get('comentario'));
            $op->setAuthorName($request->request->get('name'));
            $op->setAuthorSurname($request->request->get('surName'));
            $op->setAuthorAddress($request->request->get('address'));
            $op->setAuthorCity($request->request->get('city'));
            $op->setAuthorProvince($request->request->get('province'));
            $op->setAuthorPostalcode($request->request->get('postalCode'));
            $op->setAuthorCountry($request->request->get('country'));


            //lo persistimos
            $doctrine->persist($op);

            //lo enviamos a la BBDD
            $doctrine->flush();

            return $this->redirectToRoute('home');
        }

        //devolver un formulario html con twig
        return $this->render('addCom.html.twig');
    }

    /**
     * @Route("/admin")
     */
    public function zonaAdmin(Request $request, EntityManagerInterface $doctrine, LoggerInterface $logger)
    {
        //obtener el repo de la clase pokemon
        $repoOpiniones = $doctrine->getRepository(Opinions::class);
        $repoGuardianes = $doctrine->getRepository(Guardians::class);
        $repoFormulario = $doctrine->getRepository(Formulario::class);


        //hacer la query
        $opiniones = $repoOpiniones->findAll();
        $guardianes = $repoGuardianes->findAll();
        $formularioReg = $repoFormulario->findAll();

        return $this->render("admin.html.twig", [
            'opiniones' => $opiniones,
            'guardianes' => $guardianes,
            'formularioReg' => $formularioReg
        ]);
    }

    /**
     * @Route("/admin/:type/:id", name="delete")
     */
    public function delete(Request $request, EntityManagerInterface $doctrine, LoggerInterface $logger)
    {
    }
}
